mod from_macro;
pub use from_macro::*;

mod ops_macro;
pub use ops_macro::*;

const PRECISION_32: i32 = 16;
const PRECISION_64: i64 = 24;
const NMASK_32: i32 = 1 << 31;
const NMASK_64: i64 = 1 << 63;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fixed32 {
    n: i32,
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Fixed64 {
    n: i64,
}

#[cfg(test)]
mod tests {
    use crate::Fixed64;

    use super::Fixed32;

    macro_rules! to_from {
        ($t:ty) => {
            let f = <$t>::from(666);
            assert_eq!(<f32>::from(f), 666.0);
            assert_eq!(<i32>::from(f), 666);
            assert_eq!(<i16>::from(f), 666);

            let f = <$t>::from(127);
            assert_eq!(<i8>::from(f), 127);

            let f = <$t>::from(-666);
            assert_eq!(<i32>::from(f), -666);
            assert_eq!(<f32>::from(f), -666.0);
        };
    }

    #[test]
    fn get_f32() {
        to_from!(Fixed32);
    }

    #[test]
    fn get_f64() {
        to_from!(Fixed64);

        let f = Fixed64::from(666);
        assert_eq!(<f64>::from(f), 666.0);

        let f = Fixed64::from(-666);
        assert_eq!(<i32>::from(f), -666);
        assert_eq!(<f64>::from(f), -666.0);
    }

    #[test]
    fn add_i8_get_f32() {
        let mut f1 = Fixed32::from(127i8);
        assert_eq!(<f32>::from(f1), 127.0);

        let f2 = Fixed32::from(666);
        assert_eq!(<f32>::from(f2), 666.0);

        let f3 = f1 + f2;
        assert_eq!(<f32>::from(f3), 127.0 + 666.0);

        let f3 = f1 + &f2;
        assert_eq!(<f32>::from(f3), 127.0 + 666.0);

        let f3 = &f1 + &f2;
        assert_eq!(<f32>::from(f3), 127.0 + 666.0);

        f1 += f2;
        assert_eq!(<f32>::from(f1), 127.0 + 666.0);
    }

    #[test]
    fn add_get_f32() {
        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(666);
        let f3 = f1 + f2;
        assert_eq!(<f32>::from(f3), 666.0 * 2.0);

        let f3 = f1 + &f2;
        assert_eq!(<f32>::from(f3), 666.0 * 2.0);

        let f3 = &f1 + &f2;
        assert_eq!(<f32>::from(f3), 666.0 * 2.0);

        f1 += f2;
        assert_eq!(<f32>::from(f1), 666.0 * 2.0);

        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(-333);
        f1 += f2;
        assert_eq!(<f32>::from(f1), 333.0);
        assert_eq!(<i32>::from(f1), 333);
    }

    #[test]
    fn sub_i8_get_f32() {
        let mut f1 = Fixed32::from(127i8);
        let f2 = Fixed32::from(3);
        let f3 = f1 - f2;
        assert_eq!(<f32>::from(f3), 127.0 - 3.0);

        f1 -= f2;
        assert_eq!(<f32>::from(f1), 127.0 - 3.0);

        let mut f1 = Fixed32::from(127i8);
        let f2 = Fixed32::from(-3);
        f1 -= f2;
        assert_eq!(<f32>::from(f1), 127.0 - -3.0);
        assert_eq!(<i32>::from(f1), 127 - -3);
    }

    #[test]
    fn sub_get_f32() {
        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(3);
        let f3 = f1 - f2;
        assert_eq!(<f32>::from(f3), 666.0 - 3.0);

        f1 -= f2;
        assert_eq!(<f32>::from(f1), 666.0 - 3.0);

        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(-3);
        f1 -= f2;
        assert_eq!(<f32>::from(f1), 666.0 - -3.0);
        assert_eq!(<i32>::from(f1), 666 - -3);
    }

    #[test]
    fn mul_get_f32() {
        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(3);
        let f3 = f1 * f2;
        assert_eq!(<f32>::from(f3), 666.0 * 3.0);

        f1 *= f2;
        assert_eq!(<f32>::from(f1), 666.0 * 3.0);

        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(-3);
        f1 *= f2;
        assert_eq!(<f32>::from(f1), -1998.0);
        assert_eq!(<i32>::from(f1), -1998);
    }

    #[test]
    fn div_get_f32() {
        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(3);
        let f3 = f1 / f2;
        assert_eq!(<f32>::from(f3), 666.0 / 3.0);

        f1 /= f2;
        assert_eq!(<f32>::from(f1), 666.0 / 3.0);

        let mut f1 = Fixed32::from(666);
        let f2 = Fixed32::from(-3);
        f1 /= f2;
        assert_eq!(<f32>::from(f1), 666.0 / -3.0);
        assert_eq!(<i32>::from(f1), 666 / -3);
    }
}
