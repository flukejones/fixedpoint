use crate::{Fixed32, Fixed64};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};

macro_rules! ops {
    ($t:ident, $inner:ty) => {
        impl Add for $t {
            type Output = $t;
            #[inline]
            fn add(self, rhs: Self) -> Self::Output {
                Self { n: self.n + rhs.n }
            }
        }

        impl Add<&$t> for $t {
            type Output = $t;
            #[inline]
            fn add(self, rhs: &Self) -> Self::Output {
                Self { n: self.n + rhs.n }
            }
        }

        impl Add for &$t {
            type Output = $t;
            #[inline]
            fn add(self, rhs: Self) -> Self::Output {
                $t { n: self.n + rhs.n }
            }
        }

        impl AddAssign for $t {
            #[inline]
            fn add_assign(&mut self, rhs: Self) {
                self.n += rhs.n;
            }
        }

        impl AddAssign<&$t> for $t {
            #[inline]
            fn add_assign(&mut self, rhs: &Self) {
                self.n += rhs.n;
            }
        }

        impl Sub for $t {
            type Output = $t;
            #[inline]
            fn sub(self, rhs: Self) -> Self::Output {
                Self { n: self.n - rhs.n }
            }
        }

        impl Sub<&$t> for $t {
            type Output = $t;
            #[inline]
            fn sub(self, rhs: &Self) -> Self::Output {
                Self { n: self.n - rhs.n }
            }
        }

        impl Sub for &$t {
            type Output = $t;
            #[inline]
            fn sub(self, rhs: Self) -> Self::Output {
                $t { n: self.n - rhs.n }
            }
        }

        impl SubAssign for $t {
            #[inline]
            fn sub_assign(&mut self, rhs: Self) {
                self.n -= rhs.n
            }
        }

        impl SubAssign<&$t> for $t {
            #[inline]
            fn sub_assign(&mut self, rhs: &Self) {
                self.n -= rhs.n
            }
        }

        impl Mul for $t {
            type Output = $t;
            #[inline]
            fn mul(self, rhs: Self) -> Self::Output {
                Self {
                    n: self.n * <$inner>::from(rhs),
                }
            }
        }

        impl Mul<&$t> for $t {
            type Output = $t;
            #[inline]
            fn mul(self, rhs: &Self) -> Self::Output {
                Self {
                    n: self.n * <$inner>::from(rhs),
                }
            }
        }

        impl Mul for &$t {
            type Output = $t;
            #[inline]
            fn mul(self, rhs: Self) -> Self::Output {
                $t {
                    n: self.n * <$inner>::from(rhs),
                }
            }
        }

        impl MulAssign for $t {
            #[inline]
            fn mul_assign(&mut self, rhs: Self) {
                self.n *= <$inner>::from(rhs);
            }
        }

        impl MulAssign<&$t> for $t {
            #[inline]
            fn mul_assign(&mut self, rhs: &Self) {
                self.n *= <$inner>::from(rhs);
            }
        }

        impl Div for $t {
            type Output = $t;
            #[inline]
            fn div(self, rhs: Self) -> Self::Output {
                Self {
                    n: self.n / <$inner>::from(rhs),
                }
            }
        }

        impl Div<&$t> for $t {
            type Output = $t;
            #[inline]
            fn div(self, rhs: &Self) -> Self::Output {
                Self {
                    n: self.n / <$inner>::from(rhs),
                }
            }
        }

        impl Div for &$t {
            type Output = $t;
            #[inline]
            fn div(self, rhs: Self) -> Self::Output {
                $t {
                    n: self.n / <$inner>::from(rhs),
                }
            }
        }

        impl DivAssign for $t {
            #[inline]
            fn div_assign(&mut self, rhs: Self) {
                self.n /= <$inner>::from(rhs);
            }
        }

        impl DivAssign<&$t> for $t {
            #[inline]
            fn div_assign(&mut self, rhs: &Self) {
                self.n /= <$inner>::from(rhs);
            }
        }
    };
}

ops!(Fixed32, i32);
ops!(Fixed64, i64);
