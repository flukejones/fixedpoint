use crate::{Fixed32, Fixed64, PRECISION_32, PRECISION_64};

macro_rules! from_signed32 {
    ($($t:ty)*) => ($(
        impl From<$t> for crate::Fixed32 {
            /// Value from same size as inner conatiner must be less than half::MAX
            /// and greater than half::MIN
            #[inline]
            fn from(v: $t) -> Self {
                let n = (v as i32) & crate::NMASK_32;
                // Mask to truncate
                Self { n: ((((v as i32) & i16::MAX as i32) << crate::PRECISION_32) | n) as i32 }
            }
        }

        impl From<&$t> for crate::Fixed32 {
            #[inline]
            fn from(v: &$t) -> Self {
                (*v).into()
            }
        }

        impl From<crate::Fixed32> for $t {
            #[inline]
            fn from(v: crate::Fixed32) -> Self {
                (v.n  >> crate::PRECISION_32) as $t
            }
        }

        impl From<&crate::Fixed32> for $t {
            #[inline]
            fn from(v: &crate::Fixed32) -> Self {
                (*v).into()
            }
        }
    )*)
}
from_signed32! { i8 i16 i32 }

macro_rules! from_signed64 {
    ($($t:ty)*) => ($(
        impl From<$t> for crate::Fixed64 {
            /// Value from same size as inner conatiner must be less than i64::MAX
            /// and greater than i64::MIN
            #[inline]
            fn from(v: $t) -> Self {
                let n = (v as i64) & crate::NMASK_64;
                Self { n: (((v as i64) << crate::PRECISION_64) | n) as i64 }
            }
        }

        impl From<&$t> for crate::Fixed64 {
            #[inline]
            fn from(v: &$t) -> Self {
                (*v).into()
            }
        }

        impl From<crate::Fixed64> for $t {
            #[inline]
            fn from(v: crate::Fixed64) -> Self {
                (v.n >> crate::PRECISION_64) as $t
            }
        }

        impl From<&crate::Fixed64> for $t {
            #[inline]
            fn from(v: &crate::Fixed64) -> Self {
                (*v).into()
            }
        }
    )*)
}
from_signed64! { i8 i16 i32 i64 }

macro_rules! from_float {
    ($t:ty, $float:ty, $inner:ty, $mask:expr, $precision:expr) => {
        impl From<$float> for $t {
            #[inline]
            fn from(v: $float) -> Self {
                Self {
                    n: (v * (1 << $precision) as $float) as $inner,
                }
            }
        }

        impl From<$t> for $float {
            #[inline]
            fn from(v: $t) -> Self {
                (v.n / (1 << $precision)) as $float
            }
        }

        impl From<&$t> for $float {
            #[inline]
            fn from(v: &$t) -> Self {
                (*v).into()
            }
        }
    };
}

from_float!(Fixed32, f32, i32, NMASK_32, PRECISION_32);
from_float!(Fixed64, f32, i64, NMASK_64, PRECISION_64);
from_float!(Fixed64, f64, i64, NMASK_64, PRECISION_64);

#[cfg(test)]
mod tests {
    use crate::{Fixed32, Fixed64};

    macro_rules! min_max {
        ($t:ty) => {
            let f1 = Fixed32::from(<$t>::MAX);
            assert_eq!(<f32>::from(f1), <$t>::MAX as f32);

            let f1 = Fixed32::from(<$t>::MIN);
            assert_eq!(<f32>::from(f1), <$t>::MIN as f32);
        };
    }

    #[test]
    fn from_i8() {
        min_max!(i8);
    }

    #[test]
    fn from_i16() {
        min_max!(i16);
    }

    // #[test]
    // fn from_i32() {
    //     min_max!(i32);
    // }

    #[test]
    fn from_i32_max() {
        let f1 = Fixed32::from(i32::MAX);
        assert_eq!(<f32>::from(f1), i16::MAX as f32);
    }

    #[test]
    fn from_i32_min() {
        let f1 = Fixed32::from(i32::MIN);
        assert_eq!(<f32>::from(f1), i16::MIN as f32);
    }

    #[test]
    fn from_i64_max() {
        let f1 = Fixed64::from(i64::MAX);
        assert_eq!(<f64>::from(f1), i32::MAX as f64);
    }

    #[test]
    fn from_i64_min() {
        let f1 = Fixed64::from(i64::MIN);
        assert_eq!(<f64>::from(f1), i32::MIN as f64);
    }

    // #[test]
    // #[should_panic]
    // fn from_i32_max() {
    //     let f1 = Fixed32::from(i32::MAX);
    //     assert_eq!(<f32>::from(f1), i16::MAX as f32);
    // }

    // #[test]
    // #[should_panic]
    // fn from_i32_min() {
    //     let f1 = Fixed32::from(i32::MIN);
    //     assert_eq!(<f32>::from(f1), i16::MIN as f32);
    // }
}
